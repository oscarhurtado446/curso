<?php
//Almacena en un array los 10 primeros números pares. Imprimelos cada uno en una linea
   $pares = [2,4,6,8,10,12,14,16,18,20];
//recorrer este array con foreach
   $asociativo = [1=>90,30=>70,'e'=>99,'hola'=>43];
// no mostrar los meses donde no se hayan visto peliculas
   $contpeliculas = ['enero' => 5 , 'febrero' => 9 ,'marzo'=> 0,'abril'=>12,'marzo'=>0];

// mostrar el array con printr
//             [0]   [1]    [2][3]
   $printr = ['ana','pedro',34,1];
   print_r($printr);
   echo "<br>----------------------------<br>";


   //  foreach array pares

   foreach ($pares  as $value) {
     echo "<li>" .$value ."</li>";
   }

   echo "---------------------------- <br>";
 //  foreach array asociativo
   foreach ($asociativo as $value) {

      echo "<li>" . $value ."</li>";
   }
echo "---------------------------- <br>";

 // forech array contpeliculas
 echo "los meses en los que se vieron peliculas fueron <br>";
   foreach ($contpeliculas as $key => $value) {

      if ($value !=0) {

          echo $key."<br>";
       }
 }

 ?>
