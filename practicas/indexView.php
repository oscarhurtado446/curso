<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Practica Formuladio</title>
    <link href="https://fonts.googleapis.com/css?family=Baloo+Thambi|Concert+One" rel="stylesheet">
    <link rel="stylesheet" href="estilo.css">
  </head>
  <body>
     <div class="contenedor">
       <h1 align="center">FORMULARIO EMAIL</h1>
       <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>"method="post"> <!--De esta forma se redirige al usuario
                                                                             a la misma pagina una vez envie los datos
                                                                              si no se especifica el method se toma el get por defecto-->
        <input type="text" class="form-control" id="nombre "name="nombre" placeholder="Ingrese nombre" value=""> <!--campo para nombre-->
        <input type="text" class="form-control" id="correo" name="correo" placeholder="Ingrese correo" value=""> <!--campo para correo-->
        <textarea name="mensaje" class="form-control" id="mensaje" placeholder="Mensaje"></textarea>  <!-- campo para agregar mensaje -->

        <?php  if (!empty($errores)):?> <!--si hay algo en $errores ejecute-->
        <div class="alert error">
           <?php echo $errores; ?>
        </div>
      <?php elseif($enviado):?> <!-- ¿enviado = true ?-->
        <div class="alert success">
          <p> Enviado correctamente </p>
        </div>
      <?php endif ?>
        <input type="submit" name="btn-enviar" value="Enviar email" class="btn-enviar">  <!--Botón para enviar la informació-->
      </form>
     </div>
  </body>
</html>
