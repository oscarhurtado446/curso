<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
$errores ='';
$enviado='';
if (isset($_POST['btn-enviar'])) { /* con esto comprobamos que se haya recibido algún valor
                               o simplemente que se haya dado clien en el botón enviar  */
  $nombre = $_POST['nombre'];
  $correo = $_POST['correo'];
  $mensaje = $_POST['mensaje'];

  if (!empty($nombre)) { /* si no está vacia ejecute*/
   $nombre = trim($nombre); /*trim quita los espacios sobrantes*/
    $nombre = filter_var($nombre, FILTER_SANITIZE_STRING);/*quita los caracteres que no nos sirven*/
  }else {
    $errores .= 'Por favor ingresa un nombre <br />';
  }

  if(!empty($correo)){
    $correo = trim($correo);
    $correo = filter_var($correo,FILTER_SANITIZE_EMAIL);/*limpia el correo de caracteres no validos*/
     if(!filter_var($correo,FILTER_VALIDATE_EMAIL)){/*Devuelve un true o false dependiendo si el correo es valido o no*/
         $errores .='Por favor ingresa un correo valido <br />';
      }
    }else {
      $errores .= 'Por favor ingrese un correo <br />';
    }

     if (!empty($mensaje)) { // con este if lo que se hace es limpiar el contenido del mensaje para evitar que el usuario envíe codigo html
      $mensaje = htmlspecialchars($mensaje);/*Quita los caracteres que html tiene como especiales*/
      $mensaje = trim($mensaje);
      $mensaje = stripslashes($mensaje);// quita las barras diagonales del mensaje

    }else {
      $errores .='Por favor ingrese un mensaje <br />';
    }

    if (!$errores){
      $enviar_a = 'oscarhurtado446@gmail.com';// correo al que iría el Mensaje
      $asunto = 'Enviando correo';
      $mensaje_preparado = "De: $nombre \n";
      $mensaje_preparado .="Correo: $correo \n";
      $mensaje_preparado .="Mensaje $mensaje";

    //  mail($enviar_a,$asunto,$mensaje_preparado);//Esta es la funcion para enviar un Correo
      $enviado = 'true';
    }
}

require 'indexView.php'; /*Llama un archivo externo, en caso de que haya un
                        eror este no procesará el resto de codigo*/


 ?>
